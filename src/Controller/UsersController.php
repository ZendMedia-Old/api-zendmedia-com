<?php

namespace App\Controller;

/**
 * Description of MediaController
 *
 * @author Rauxmedia
 */
class UsersController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
    }


    public function profile($id) {
        $this->responseArray = $this->Users->profile($id);
        $this->json();
    }

}

?>
