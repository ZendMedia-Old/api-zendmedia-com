<?php

namespace App\Controller;

/**
 * Description of MediaController
 *
 * @author Rauxmedia
 */
class MediaController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel("MediaChannelNodes");
    }

    public function index() {
        
    }

    public function content($id) {
        $this->responseArray = $this->Media->content($id);
        $this->json();
    }

}

?>
