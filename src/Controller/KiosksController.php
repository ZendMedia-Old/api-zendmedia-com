<?php

namespace App\Controller;

/**
 * Description of KiosksController
 *
 * @author Rauxmedia
 */
class KiosksController extends AppController {

    public function identify($identifier) {
        $this->responseArray = $this->Kiosks->identify($identifier);
        $this->json();
    }

    public function settings($id) {
        $this->responseArray = $this->Kiosks->settings($id);
        $this->json();
    }
    
    public function content($id) {
        $this->responseArray = $this->Kiosks->content($id);
        $this->json();
    }
    
    

}

?>
