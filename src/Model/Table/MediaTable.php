<?php

namespace App\Model\Table;

use \Cake\ORM\TableRegistry;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MediaTable
 *
 * @author Rauxmedia
 */
class MediaTable extends ZendmediaTable {

    public function initialize(array $config) {
        parent::initialize($config);
        $this->table("media_channel_nodes");
        $this->addAssociations([
            'belongsTo' => ['MediaNodes']
        ]);
    }
    
    public function content($id = null){
        $result = array("success" => false, "message" => "Not found");
        if ($id != null) {
            $query = $this->findByMediaChannelId($id);
            $query->contain(["MediaNodes"]);
            $result = array("success" => true, "data" => $query->all());
        }
        return $result;
    }

}

?>
