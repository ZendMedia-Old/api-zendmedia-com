<?php
namespace App\Model\Table;
/**
 * Description of KiosksTable
 *
 * @author Rauxmedia
 */
class KioskMediaChannelsTable extends ZendmediaTable {

    public function initialize(array $config) {
        parent::initialize($config);
        $this->addAssociations([
            'belongsTo' => ['MediaChannels']
        ]);
    }

    

}

