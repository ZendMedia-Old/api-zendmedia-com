<?php
namespace App\Model\Table;

use \Cake\ORM\TableRegistry;
/**
 * Description of KiosksTable
 *
 * @author Rauxmedia
 */
class KiosksTable extends ZendmediaTable {

    public function initialize(array $config) {
        parent::initialize($config);
        $this->table("hardware_nodes");
    }

    public function identify($identifier) {
        $result = array("success" => false, "message" => "Not found");
        $query = $this->findByIdentifier($identifier);
        $query->select(["id", "name"]);
        $kiosk = $query->first();
        if (!empty($kiosk)) {
            $result = array("success" => true, "data" => $kiosk);
        }
        return $result;
    }

    public function settings($id = null) {
        $result = array("success" => false, "message" => "Not found");
        if ($id != null) {
            $query = $this->findById($id);
            $query->select(["id", "name", "identifier", "object"]);
            $kiosk = $query->first();
            if (!empty($kiosk)) {
                
                $settingsTable = TableRegistry::get("KioskSettings");
                $settings = $settingsTable->findByKioskId($kiosk->id);
                $settings->select(["content", "smurkz", "juicebox"]);
                $kiosk["settings"] = $settings->first();
                
                $result = array("success" => true, "data" => $kiosk);
            }
        }
        return $result;
    }
    
    public function content($id = null) {
        $result = array("success" => false, "message" => "Not found");
        if ($id != null) {
            $kioskChannelsTable = TableRegistry::get("KioskMediaChannels");
            $query = $kioskChannelsTable->findByKioskId($id);
            $query->contain(["MediaChannels.MediaChannelPresets"]);
            $channels = $query->all();
            if (!empty($channels)) {
                $result = array("success" => true, "data" => $channels);
            }
        }
        return $result;
    }

}

?>
