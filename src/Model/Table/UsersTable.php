<?php

namespace App\Model\Table;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MediaTable
 *
 * @author Rauxmedia
 */
class UsersTable extends ZendmediaTable {

    public function initialize(array $config) {
        parent::initialize($config);
    }
    
    public function profile($id = null){
        $result = array("success" => false, "message" => "Not found");
        if ($id != null) {
            $query = $this->findById($id);
            $result = array("success" => true, "data" => $query->first());
        }
        return $result;
    }

}
