<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Model\Table;
/**
 * Description of ZendmediaTable
 *
 * @author Rauxmedia
 */

use Cake\ORM\Table;

class ZendmediaTable extends Table {
    
    public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'modified_at' => 'always'
                ]
            ]
        ]);
    }
    
    
    protected function create($request_data){
        $entity = $this->newEntity($request_data);
        if($this->save($entity)){
            return $entity->id;
        }else{
            return null;
        }
            
    }
}

?>
