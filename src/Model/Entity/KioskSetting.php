<?php
namespace App\Model\Entity;
use Cake\ORM\Entity;
/**
 * Description of KioskSetting
 *
 * @author Rauxmedia
 */
class KioskSetting extends Entity{
    protected function _getSmurkz($smurkz)
    {
        return json_decode($smurkz);
    }
}
